#+TITLE:      Environment
#+AUTHOR:     Toon Claes
#+KEYWORDS:   toon claes profile environment config
#+STARTUP:    showall
#+PROPERTY:   header-args+ :comments both
#+PROPERTY:   header-args+ :mkdirp yes
#+PROPERTY:   header-args+ :tangle "~/.config/environment.d/envvars.conf"
-----

* System wide

** Load user environment from profile

#+begin_src sh :tangle no
#+begin_src sh :tangle (if noninteractive "no" "/sudo::/etc/profile.d/env.sh")
# User specified environment
if [ -d ~/.config/environment.d ]; then
	for rc in ~/.config/environment.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

unset rc
#+end_src

* Environment.d

Systemd's environment.d(5) will read all variables from
=~/.config/environment.d/*.conf=. This section writes variables to that file.

** Define XDG Home dirs

Make sure the =XDG_= variables are set:

#+begin_src sh
XDG_DATA_HOME="$HOME/.local/share"
XDG_CONFIG_HOME="$HOME/.config"
XDG_CACHE_HOME="$HOME/.cache"
#+end_src

** Extend =$PATH=

#+begin_src sh
PATH="$HOME/.local/bin":$PATH
#+end_src

** Default tools

*** Editor

Emacs is the =$EDITOR=.

#+BEGIN_SRC sh
EDITOR="emacsclient"
#+END_SRC

*** Terminal

Set the preferred terminal emulator.

#+begin_src sh
TERMINAL=/usr/bin/alacritty
#+end_src

*** Zsh

Make zsh find it's RC files. Although zsh will not find the environments file
automagically.

#+begin_src sh
ZDOTDIR="$XDG_CONFIG_HOME/zsh"
#+end_src

** Tools

*** Docker

#+begin_src sh :tangle no
DOCKER_CONFIG="$XDG_CONFIG_HOME/docker"
MACHINE_STORAGE_PATH="$XDG_DATA_HOME/docker-machine"
#+end_src

*** Golang

#+begin_src sh
GOPATH="$XDG_DATA_HOME/go"
#+end_src

And add it to the =PATH=.

#+begin_src sh
PATH=$GOPATH/bin:$PATH
#+end_src

*** Rust

#+begin_src sh
CARGO_HOME="$XDG_DATA_HOME/cargo"
#+end_src

And add it to the =PATH=.

#+begin_src sh
PATH=$CARGO_HOME/bin:$PATH
#+end_src

**** Rustup

#+begin_src sh
RUSTUP_HOME="$XDG_CONFIG_HOME/rustup"
#+end_src

*** Yarn

Add Yarn bin dir to =PATH=.

#+begin_src sh :tangle no
command -v yarn > /dev/null && export PATH=$(yarn global bin):$PATH
#+end_src

*** GnuPG

#+begin_src sh
GNUPGHOME="$XDG_DATA_HOME/gnupg"
#+end_src

*** Gem

No longer needed, because =mise= handles that.

#+begin_src sh :tangle no
GEM_HOME="${XDG_DATA_HOME}"/gem
GEM_SPEC_CACHE="$XDG_CACHE_HOME/gem"
#+end_src
*** IRB                                                                :ruby:

#+begin_src sh
IRBRC="$XDG_CONFIG_HOME/irb/irbrc"
#+end_src

*** Bundler                                                            :ruby:

#+begin_src sh
BUNDLE_USER_CONFIG="$XDG_CONFIG_HOME/bundle"
BUNDLE_USER_CACHE="$XDG_CACHE_HOME/bundle"
BUNDLE_USER_PLUGIN="$XDG_DATA_HOME/bundle"
BUNDLE_PATH="vendor/bundle"
#+end_src

*** Solargraph

#+begin_src sh
SOLARGRAPH_CACHE="$XDG_CACHE_HOME/solargraph"
#+end_src

*** Less

#+begin_src sh
LESSKEY="$XDG_CONFIG_HOME/less/lesskey"
LESSHISTFILE="$XDG_CACHE_HOME/less/history"
#+end_src

*** GDB

#+begin_src sh
GDBHISTFILE="$XDG_DATA_HOME"/gdb/history
#+end_src

*** Vim

#+begin_src sh
VIMINIT="set viminfo+='1000,n$XDG_DATA_HOME/vim/viminfo'"
#+end_src

*** PostgreSQL

#+begin_src sh
PSQLRC="$XDG_CONFIG_HOME/pg/psqlrc"
PSQL_HISTORY="$XDG_CACHE_HOME/pg/psql_history"
PGPASSFILE="$XDG_CONFIG_HOME/pg/pgpass"
#PGSERVICEFILE="$XDG_CONFIG_HOME/pg/pg_service.conf"
#+end_src

*** Asdf

[[https://asdf-vm.com/][=asdf=]] is used by the [[https://gitlab.com/gitlab-org/gitlab-development-kit][gitlab-development-kit]].

#+begin_src sh
ASDF_CONFIG_FILE="$XDG_CONFIG_HOME/asdf/asdfrc"
ASDF_DATA_DIR="$XDG_DATA_HOME/asdf"
#+end_src

*** Ansible

#+begin_src sh
ANSIBLE_HOME="$XDG_DATA_HOME/ansible"
#+end_src

*** Lolcommits

Tell lolcommits which device to use: HD Pro Webcam C920.

Check which devices are available with:

#+begin_src sh :tangle no :output results
v4l2-ctl --list-devices
#+end_src

#+RESULTS:
| Integrated | IR          | Camera:    | Integrate | (usb-0000:00:14.0-5):   |
|            | /dev/video0 |            |           |                         |
|            | /dev/video1 |            |           |                         |
| Integrated | Camera:     | Integrated | C         | (usb-0000:00:14.0-8):   |
|            | /dev/video2 |            |           |                         |
|            | /dev/video3 |            |           |                         |
| HD         | Pro         | Webcam     | C920      | (usb-0000:3c:00.0-1.2): |
|            | /dev/video4 |            |           |                         |
|            | /dev/video5 |            |           |                         |


#+BEGIN_SRC sh :tangle no
LOLCOMMITS_DEVICE=$(v4l2-ctl --list-devices | grep -A1 C920 | tail -n1 | tr -d "\s\t")
#+END_SRC

*** Qt & KDE

Make sure the [[https://wiki.archlinux.org/index.php/Dolphin#Icons_not_showing][icons are shown]]:

#+begin_src sh :tangle no
[ "$XDG_CURRENT_DESKTOP" = "KDE" ] || [ "$XDG_CURRENT_DESKTOP" = "GNOME" ] || export QT_QPA_PLATFORMTHEME="qt5ct"
#+end_src

And that they are not too big:

#+begin_src sh :tangle no
#XDG_CURRENT_DESKTOP=KDE
KDE_SESSION_VERSION=5
QT_AUTO_SCREEN_SCALE_FACTOR=0
QT_FONT_DPI=120
#+end_src

*** Password store

Encrypt the passwords in password store ASCII armored.

#+BEGIN_SRC sh
PASSWORD_STORE_GPG_OPTS=--armor
#+END_SRC

*** Notmuch

Locate ~notmuch~ config:

#+begin_src sh
NOTMUCH_CONFIG=$XDG_CONFIG_HOME/notmuch/config
#+end_src

** Wayland

From: https://github.com/swaywm/sway/issues/5008#issuecomment-665972776

#+begin_src sh
# Needed for xdg-desktop-portal
XDG_CURRENT_DESKTOP=sway
#+end_src

#+begin_src sh :tangle no
# this may fix external monitor problem
WLR_DRM_NO_MODIFIERS=1

# force wayland backend for bemenu
BEMENU_BACKEND=wayland

# Force Wayland backend for qt5 apps
QT_QPA_PLATFORM=wayland
QT_WAYLAND_DISABLE_WINDOWDECORATION=1

# general
XDG_SESSION_TYPE=wayland
#+end_src

*** Firefox

Force Firefox to use Wayland.

#+begin_src sh
MOZ_DBUS_REMOTE=1
MOZ_ENABLE_WAYLAND=1
#+end_src

# Local Variables:
# eval: (auto-fill-mode 1)
# eval: (flyspell-mode  1)
# End:
